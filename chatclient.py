#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import json
import sys
from configuration import *

serverlast = 0
clientlast = 0
awaymessages = 0
away = False
cookie = None

if server == "chat.asparg.us":
    ssloption =  False
else:
    ssloption = False

def login():
    global cookie
    r = requests.post("https://"+server+"/api", data={"email": email, "password": password}, verify=ssloption)
    if r.text == "loggedin":
        print "login successful!"
        #print r.cookies
        cookie = r.cookies
        return 
    elif r.text == "wrongcred":
        print "wrong credentials"
        sys.exit(0)
    elif r.text == "notvalidated":
        print "not validated"
        sys.exit(0)
    elif r.text == "error":
        print "error"
        sys.exit(0)

def sessionstarter():
    getlast = requests.post("https://"+server+"/api", data={"getlast": ""}, cookies=cookie, verify=ssloption)
    print "last message: ",
    print getlast.text
    users = requests.post("https://"+server+"/api", data={"getstatuses": ""}, cookies=cookie, verify=ssloption)
    users = eval(users.text)
    for user in users:
        print user['nick'] + ":",
        if user['state'] == 1:
            print "online"
        elif user['state'] == 2:
            print "browsing"
        elif user['state'] == 3:
            print "idle"
        elif user['state'] == 4:
            print "away"
        else:
            print "offline"
    allmessages = requests.post("https://"+server+"/api", data={"getall": ""}, cookies=cookie, verify=ssloption)
    allmessages = eval(allmessages.text)
    for msg in allmessages:
        if msg['id']>300:
            print msg['date'],
            print msg['nick'],
            print msg['msg']
    return

def pingroutine():
    cmd = raw_input('Type here: ')
    if cmd == "/quit":
        sys.exit(0)
    else:
        response = requests.post("https://"+server+"/api", data={"textfield": cmd}, cookies=cookie, verify=ssloption)
        #print response.cookies
        #print cookie
        print response.text
    return

if __name__ == "__main__":
    login()
    sessionstarter()
    while True:
        pingroutine()
