#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import json
import sys
from PyQt4.QtCore import QString
from configuration import *


serverlast = 0
clientlast = 0
awaymessages = 0
away = False
cookie = None

if server == "chat.asparg.us":
    ssloption =  False
else:
    ssloption = True

def login():
    global cookie
    r = requests.post("https://"+server+"/api", data={"email": email, "password": password}, verify=ssloption)
    if r.text == "loggedin":
        print "login successful!"
        #print r.cookies
        cookie = r.cookies
        return 
    elif r.text == "wrongcred":
        print "wrong credentials"
        sys.exit(0)
    elif r.text == "notvalidated":
        print "not validated"
        sys.exit(0)
    elif r.text == "error":
        print "error"
        sys.exit(0)

def sessionstarter():
    getlast = requests.post("https://"+server+"/api", data={"getlast": ""}, cookies=cookie, verify=ssloption)
    #print "last message: ",
    #print getlast.text
    users = requests.post("https://"+server+"/api", data={"getstatuses": ""}, cookies=cookie, verify=ssloption)
    users = eval(users.text)
    for user in users:
        print user['nick'] + ":",
        if user['state'] == 1:
            print "online"
        elif user['state'] == 2:
            print "browsing"
        elif user['state'] == 3:
            print "idle"
        elif user['state'] == 4:
            print "away"
        else:
            print "offline"
    return getlast.text

def getall():    
    allmessages = requests.post("https://"+server+"/api", data={"getall": ""}, cookies=cookie, verify=ssloption)
    allmessages = eval(allmessages.text)
    return allmessages

def posthandler(cmd):
    if cmd == "/quit":
        sys.exit(0)
    else:
        cmd = cmd.toUtf8()
        cmd = str(cmd)
        response = requests.post("https://"+server+"/api", data={"textfield": cmd}, cookies=cookie, verify=ssloption)
        return response.text

def pinger(clientlast):
        response = requests.post("https://"+server+"/api", data={"ping": clientlast}, cookies=cookie, verify=ssloption)
        return response.text

if __name__ == "__main__":
    login()
    sessionstarter()
    while True:
        pingroutine()
