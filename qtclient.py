#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt4.QtCore import Qt, QTimer, QString
from PyQt4.QtGui import (
     QApplication, QWidget, QLabel, QLineEdit, QTextEdit,
     QFrame, QGridLayout, QVBoxLayout, QAction
     )
import sys
import json
from chatlib import *

class Initialize():
    def __init__(self):
        self.initchat()

    def initchat(self):
        app = QApplication(sys.argv)
        w = QWidget()
        w.resize(400, 200)
        w.frame = QFrame(w)

        #self.frame.setFrameShape(QFrame.Box)
        grid = QGridLayout(w.frame)
        self.chatbox = QTextEdit(w.frame)
        self.chatinput = QLineEdit(w.frame)
        grid.addWidget(self.chatbox, 0, 1)
        grid.addWidget(self.chatinput, 1, 1)
        vbox = QVBoxLayout(w)
        vbox.addWidget(w.frame)
        vbox.addStretch()
    
        login()
        global clientlast
        clientlast = sessionstarter()
        allmessages = getall()
        for msg in allmessages:
            if msg['id']>300:
                self.chatbox.append(str(msg['date']) + " " + str(msg['nick']) + " " + msg['msg'])
        self.my_timer = QTimer()
        self.my_timer.timeout.connect(self.pingroutine)
        self.my_timer.start(2000) #1 second interval
        
        w.show()
        self.chatinput.setFocus()
        self.chatinput.returnPressed.connect(self.postrequest)
        #pinger()
        sys.exit(app.exec_())

    def postrequest(self):
        msg = self.chatinput.text()
        #print type(msg)
        response = posthandler(msg)
        self.chatinput.clear()
        self.chatbox.append(response)

    def pingroutine(self):
        global clientlast
        check = pinger(clientlast)
        if check == "0":
            return
        else:
            true = True
            false = False
            check = eval(check)
            for i in check:
                if i['own']:
                    clientlast = int(clientlast) + 1
                if not i['own']:
                    self.chatbox.append(str(i['date']) + " " + str(i['nick']) + " " + str(i['msg']))        
                    clientlast = int(clientlast) + 1

if __name__ == "__main__":
    Initialize()
